#!/bin/sh
PROJECT=infrastructure-prod-321111
CLUSTER=cluster-1
ZONE=us-central1-c
name=bitwarden

gcloud container clusters get-credentials $CLUSTER --zone $ZONE --project $PROJECT

# Removal of deployment, pod, services, certificate, ingress.
kubectl -n $name delete statefulset,deployment,po,svc,managedcertificate,ingress --all

# Releasing the reserved static address for ingress.
yes|gcloud compute addresses delete $name --global