#!/bin/sh
PROJECT=infrastructure-prod-321111
CLUSTER=cluster-1
ZONE=us-central1-c
name=bitwarden
cf_api_key="f4Nbb1d-ppG-iR7dLjWn6u0lLeEcIxXlLf8dMXIx"
sub_domain=bw
domain=garantyr.io
api_url="https://api.cloudflare.com/client/v4/zones"
domain_zone="d4992d4a5b581d554cce9b69d35e43d8"

gcloud container clusters get-credentials $CLUSTER --zone $ZONE --project $PROJECT

gcloud config set project $PROJECT

static_ip_in_gcp="$(gcloud compute addresses list | grep "$name" | awk '{print $2}')"

# If the static IP of the domain is not reserved in GCP, then we reserve it. 
if [ -z "$static_ip_in_gcp" ]; then {
echo "\n---В GCP IP $sub_domain.$domain not reserved. Reserving."
gcloud compute addresses create $name --project=$PROJECT --global
static_ip_in_gcp="$(gcloud compute addresses list | grep "$name" | awk '{print $2}')"
}
fi

kubectl apply -f 00-namespace.yaml 
kubectl apply -f configmap.yml -n $name
kubectl apply -f rbac.yml -n $name
kubectl apply -f statefulset.yml -n $name
kubectl apply -f smtp-secret.yml -n $name
kubectl apply -f service.yml -n $name
kubectl apply -f ingress.yml -n $name

kubectl rollout restart StatefulSets $name -n $name

echo "\n--- ManagedCertificate:"
kubectl get ManagedCertificate -n $name

echo "\n--- Compute addresses list $name:"
gcloud compute addresses list --filter $name

echo "\n--- In GCP reserved IP $sub_domain.$domain: $static_ip_in_gcp"

# We look at the IP address of the domain в CLOUDFLARE | docker image https://github.com/danielpigott/cloudflare-cli/
ip_in_cf="$(docker run --rm -it -e CF_API_KEY=$cf_api_key -e CF_DOMAIN=$domain dpig/cloudflare-cli ls|grep $sub_domain.$domain | awk '{print $6}')"
echo "\n--- В CLOUDFLARE IP $sub_domain.$domain: $ip_in_cf"

# If the IP in GCP and CLOUDFLARE are different, then you need to change the IP address in CLOUDFLARE.
if [ "$static_ip_in_gcp" != "$ip_in_cf" ]; then {
echo "\n--- В CLOUDFLARE изменяю IP DNS для $sub_domain.$domain на IP из GCP."

# Getting the domain's dns_record in CLOUDFLARE
dns_record="$(docker run --rm -it -e CF_API_KEY=$cf_api_key -e CF_DOMAIN=$domain dpig/cloudflare-cli ls|grep $sub_domain.$domain | awk '{print $(NF-1)}')"
echo "\n--- dns_record $sub_domain: $dns_record"

# Changing the IP address for a domain in CLOUDFLARE
curl -X PUT "$api_url/$domain_zone/dns_records/$dns_record" \
      -H "Authorization: Bearer $cf_api_key" \
      -H "Content-Type: application/json" \
      --data '{"type":"A","name":"'$sub_domain'.'$domain'","content":"'$static_ip_in_gcp'","proxied":false}'

# We look at the IP address of the domain in CLOUDFLARE
ip_in_cf="$(docker run --rm -it -e CF_API_KEY=$cf_api_key -e CF_DOMAIN=$domain dpig/cloudflare-cli ls|grep $sub_domain.$domain | awk '{print $6}')"
echo "\n--- В CLOUDFLARE IP $sub_domain.$domain: $ip_in_cf"

} else {
echo "\n--- IP in GCP ($static_ip_in_gcp) == IP in CLOUDFLARE ($ip_in_cf). IP the addresses are the same."
}
fi